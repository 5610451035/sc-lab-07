
public class Scheule 
{
	SeatPrices seats1;
	SeatPrices seats2;
	SeatPrices seats3;
	
	public Scheule()
	{
		seats1 = new SeatPrices();
		seats2 = new SeatPrices();
		seats3 = new SeatPrices();
	}
	
	
	public DataSeatPrice getTheaterNo(String no)
	{
		if (no == "Theater 1")
		{
			return seats1;
		}
		
		else if (no == "Theater 2")
		{
			return seats2;
		}
		
		else
		{
			return seats3;
		}
		
	}
}
