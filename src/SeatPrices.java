
public class SeatPrices implements DataSeatPrice
{
	private int[][] seatPrices = copy(ticketPrices);
	
	@Override
	public int[][] getSeatsPrices() 
	{
		return seatPrices;
	}

	@Override
	public void setSeatPrice(int i, int j , int price) 
	{
		seatPrices[i][j] = price;
	}

	@Override
	public void restartSeats() 
	{
		seatPrices = ticketPrices.clone();
	}

	@Override
	public int getSeatPrice(int i , int j) {
		
		return seatPrices[i][j];
	}
	
	public int[][] copy(int[][] origin)
	{
		int[][] copy = new int[15][20];
		for (int i = 0; i < origin.length ; i++)
		{
			for (int j = 0 ; j < origin[i].length ; j++)
			{
				copy[i][j] = origin[i][j];
			}
		}
		return copy;
	}
	
	
}