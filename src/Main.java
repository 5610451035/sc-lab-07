
public class Main 
{
	public static void main(String[] args)
	{
		new Main();
	}
	
	public Main()
	{
		Seats seats = new Seats();
		Scheule scheule = new Scheule();
		GUI gui = new GUI();
		TheaterManagement controller = new TheaterManagement(seats, scheule, gui);
		gui.setSeats();
	}
}
