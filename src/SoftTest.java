
public class SoftTest {

	public static void main(String[] args) 
	{
		new SoftTest();
		
	}
	
	public SoftTest()
	{
		Seats seats = new Seats();
		Scheule scheule = new Scheule();
		GUI gui = new GUI();
		TheaterManagement controller = new TheaterManagement(seats, scheule, gui);
		testCase(controller , gui);
		gui.setSeats();
	}
	
	public void testCase(TheaterManagement controller , GUI gui)
	{
		controller.buySeat(3, 11);
		controller.buySeat(3, 12);
		gui.setText(controller.searchPrices("20"));
		gui.setText(controller.searchPrices("50"));
		controller.buySeat(3, 12);
		controller.buySeat(3, 13);
		
	}

}
