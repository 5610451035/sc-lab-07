import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;


public class GUI extends JFrame implements ActionListener
{
	private TheaterManagement controller;
	
	private JPanel panelLeft;
	private JPanel panelRight;

	private JComboBox theaterNo;
	
	private JButton buy;
	private JButton search;
	private JButton free;
	private JButton clear;
	private JButton end;
	
	private JTextArea seatPrices;
	private JTextField row;
	private JTextField column;
	private JTextField priceSearch;
	
	private JLabel totalPrices;
	
	public GUI()
	{
		startGUI();
	}
	
	public void startGUI()
	{
		pack();
		createGUI();
		setSize(800, 600);
		addListener();
		setVisible(true);
		
	}
	
	public void createGUI()
	{
		panelLeft = new JPanel();
		panelLeft.setBorder(new TitledBorder("Seats Show : "));
		
		panelRight = new JPanel();
		panelRight.setLayout(new GridLayout(10,2));
		panelRight.setPreferredSize(new Dimension(300,600));
		
		theaterNo = new JComboBox();
		theaterNo.addItem("Theater 1");
		theaterNo.addItem("Theater 2");
		theaterNo.addItem("Theater 3");
		
		buy = new JButton("Buy");
		search = new JButton("Search By Price");
		clear = new JButton("Clear");
		end = new JButton("End Program");
		free = new JButton("Search for Free Seats");
		
		seatPrices = new JTextArea();
		row = new JTextField("Enter row");
		column = new JTextField("Enter column");
		priceSearch = new JTextField("Enter Price for Search");

		totalPrices = new JLabel("Total Price : ");
		
		add(panelLeft , BorderLayout.CENTER);
		add(panelRight , BorderLayout.EAST);
		
		panelLeft.add(seatPrices,BorderLayout.CENTER);
		
		panelRight.add(theaterNo);
		panelRight.add(priceSearch);
		panelRight.add(search);
		panelRight.add(row);
		panelRight.add(column);
		panelRight.add(buy);
		panelRight.add(totalPrices);
		panelRight.add(free);
		panelRight.add(clear);
		panelRight.add(end);
		
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if (e.getSource() == theaterNo)
		{
			setSeats();
		}
		
		if (e.getSource() == search)
		{
			setSearchSeats();
		}
		
		if (e.getSource() == buy)
		{	
			controller.getPriceAt((int)(row.getText().toUpperCase().charAt(0)-65), Integer.parseInt(column.getText())-1);
			totalPrices.setText("Total Price : " + controller.getTotalPrices());
			controller.buySeat((int)(row.getText().toUpperCase().charAt(0)-65), Integer.parseInt(column.getText())-1);
			setSeats();
		}
		
		if (e.getSource() == free)
		{
			seatPrices.setText(controller.searchFreeSeats());
		}
		
		if (e.getSource() == end)
		{
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);
		}
		
		if (e.getSource() == clear)
		{
			controller.cancleSeats();
			controller.clearAmount();
			totalPrices.setText("Total Price : ");
			setSeats();
		}
		
	}
	
	public void getController(TheaterManagement controller)
	{
		this.controller = controller;
	}
	
	public String getTheaterNo()
	{
		return theaterNo.getSelectedItem().toString();
	}
	
	public void setSeats()
	{
		controller.getTheaterNo();
		seatPrices.setText(controller.getPrices());
	}
	
	public void setSearchSeats()
	{
		controller.getTheaterNo();
		seatPrices.setText(controller.searchPrices(priceSearch.getText()));
	}
	
	public void setText(String str)
	{
		controller.getTheaterNo();
		seatPrices.setText(str);
	}

	public void addListener()
	{
		theaterNo.addActionListener(this);
		buy.addActionListener(this);
		search.addActionListener(this);
		priceSearch.addActionListener(this);
		row.addActionListener(this);
		column.addActionListener(this);
		clear.addActionListener(this);
		end.addActionListener(this);
		free.addActionListener(this);
	}
	
}
