import java.util.ArrayList;


public class TheaterManagement 
{
	Scheule scheule;
	DataSeatPrice seatPrices;
	Seats seats;
	GUI gui;
	
	public TheaterManagement(Seats seats, Scheule scheule, GUI gui)
	{
		this.scheule = scheule;
		this.seats = seats;
		this.gui = gui;
		getTheaterNo();
		gui.getController(this);
	}
	
	public void getTheaterNo()
	{
		this.seatPrices = scheule.getTheaterNo(gui.getTheaterNo());
	}
	
	public String getPrices()
	{
		int[][] prices = seats.getPrices(seatPrices);
		String seats = "";
		char row = 'A';
		int column = 1;
		
		for(int[] i : prices)
		{
			if (row == 'I')
			{
				seats = seats + row + "    ";
			}
			
			else
			{
				seats = seats + row + "  ";
			}
			for (int j : i)
			{
				if (j == -1)
				{
					seats = seats + "       ";
				}
				
				else 
				{
					seats = seats + j + "  ";
				}
				column = column + 1;
			}
			
			row = (char)((int)row + 1);
			seats = seats + "\n\n";
		}
		return seats + "     1    2    3    4     5    6     7    8    9    10  11  12  13  14  15  16  17  18  19   20 ";
	}
	
	public String searchPrices(String price)
	{
		String searchByPrice = getPrices().replace("     1    2    3    4     5    6     7    8    9    10  11  12  13  14  15  16  17  18  19   20 ", "") ;
		String[] prices = {"10","20","30","40","50"};
		
		for (String i : prices)
		{
			if(!i.contains(price))
			{
				searchByPrice = searchByPrice.replaceAll(i, "    ");
			}
			
		}
		
		return searchByPrice + "     1    2    3    4     5    6     7    8    9    10  11  12  13  14  15  16  17  18  19   20 ";
	}
	
	public String searchFreeSeats()
	{
		String freeSeats = getPrices().replaceAll(" 0 ", "    ");
		return freeSeats;
	}
	
	public void getPriceAt(int i , int j)
	{
		seats.getPrice(i, j,seatPrices);
	}
	
	public String getTotalPrices()
	{
		return Integer.toString(seats.getTotalPrice());
	}
	
	public void setPrices(int i , int j , int price) 
	{
		seats.setPrices(i, j, price,seatPrices);
	}
	
	public void buySeat(int i , int j)
	{
		seats.buySeat(i, j, seatPrices);
	}
	
	public void cancleSeat(int i , int j)
	{
		seats.cancleSeat(i, j,seatPrices);
	}
	
	public void cancleSeats()
	{
		seats.cancleSeats(seatPrices);
	}
	
	public void clearAmount()
	{
		seats.clearAmount();
	}
}
