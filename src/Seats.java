
public class Seats 
{	
	private int amount = 0;
	public int[][] getPrices(DataSeatPrice seats)
	{
		return seats.getSeatsPrices();
	}
	
	public int getPrice(int i , int j , DataSeatPrice seats)
	{
		if (seats.getSeatPrice(i, j) != -1)
		{
			amount = amount + seats.getSeatPrice(i, j);
		}
		
		return seats.getSeatPrice(i, j);
	}
	
	public int getTotalPrice()
	{
		return amount;
	}
	
	public void clearAmount()
	{
		amount = 0;
	}
	
	public void setPrices(int i , int j , int price , DataSeatPrice seats) 
	{
		seats.setSeatPrice(i, j, price);
	}
	
	public void buySeat(int i , int j , DataSeatPrice seats)
	{
		if (seats.getSeatPrice(i, j) != -1)
		{
			seats.setSeatPrice(i, j, 0);
		}
	}
	
	public void cancleSeat(int i , int j , DataSeatPrice seats)
	{
		seats.setSeatPrice(i, j, DataSeatPrice.ticketPrices[i][j]);
	}
	
	public void cancleSeats(DataSeatPrice seats)
	{
		seats.restartSeats();
	}
	
	
}